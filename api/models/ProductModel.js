'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//Does not need to match monolith in current purpose; only properties that shall be compared (showed) are useful to insert

var ProductSchema = new Schema({
  productId: {
    type: String,
    required: 'productId is required',
	unique: true
  },
  /* GenericAttributes : [name:{type:String,requiered:'name required'},value:{type:String,requiered:'name required'}], */
/*   ProductTypeId: {type: Number},
  Name : {type:String,requiered:'name required'},
  ShortDescription:{type:String},
  FullDescription:{type:String},
  Price:{type:Number, required:"price reqiured"}, */
  
	CreatedAt:{type: Date,
				default: Date.now},
	UpdatedAt:{type: Date,
				required:"UpdatedAt is required"}
}, { collection: 'Products' });

/* ProductSchema.methods.getProduct = function(_prodId){
	ProductSchema.findOne({productId:_prodId}, function(err, product) {
		if (err) return false;
		return product;
	});
}; */
/* ProductSchema.methods.isProductInSync = function(_product, _updatedDate){
	//_product !=null
	// is the dates in matching formats ? 
	if(_product.UpdatedAt!=_updatedDate) return false;
	return true;
}; */

module.exports = mongoose.model('Product', ProductSchema);


/*
	{
	 "_id" : "5a93fd35b0189b0f384a567e",	type: String
    "GenericAttributes" : [],				�type: Array�
y    "ProductTypeId" : 5,					type: Int32
y    "Name" : "Virus Protection",
    "SeName" : "virus-protection",
y    "ShortDescription" : "An precise and effective virus protection that keeps your computer cleen!",
y    "FullDescription" : "<p><em>An precise and effective virus protection that keeps your computer cleen!</em></p>",
    "AdminComment" : "clinical",
    "ProductTemplateId" : "5a90116c77eb6c43c8f05602",
    "VendorId" : null,
    "MetaKeywords" : null,
    "MetaDescription" : null,
    "MetaTitle" : null,
    "AllowCustomerReviews" : true,
    "ApprovedRatingSum" : 0,
    "NotApprovedRatingSum" : 0,
    "ApprovedTotalReviews" : 0,
    "NotApprovedTotalReviews" : 0,
    "SubjectToAcl" : false,
    "CustomerRoles" : [],
    "LimitedToStores" : false,
    "Stores" : [],
    "Sku" : "100",
    "ManufacturerPartNumber" : "1",
    "Gtin" : null,
    "IsGiftCard" : false,
    "GiftCardTypeId" : 0,
    "OverriddenGiftCardAmount" : null,
    "RequireOtherProducts" : false,
    "RequiredProductIds" : null,
    "AutomaticallyAddRequiredProducts" : false,
    "IsDownload" : true,
    "DownloadId" : null,
    "UnlimitedDownloads" : true,
    "MaxNumberOfDownloads" : 10,
    "DownloadExpirationDays" : 365,
    "DownloadActivationTypeId" : 0,
    "HasSampleDownload" : false,
    "SampleDownloadId" : null,
    "HasUserAgreement" : false,
    "UserAgreementText" : null,
    "IsRecurring" : true,
    "RecurringCycleLength" : 1,
    "RecurringCyclePeriodId" : 30,
    "RecurringTotalCycles" : 10,
    "IsRental" : false,
    "RentalPriceLength" : 1,
    "RentalPricePeriodId" : 0,
    "IsShipEnabled" : false,
    "IsFreeShipping" : true,
    "ShipSeparately" : true,
    "AdditionalShippingCharge" : 0.0,
    "DeliveryDateId" : null,
    "IsTaxExempt" : false,
    "TaxCategoryId" : null,
    "IsTelecommunicationsOrBroadcastingOrElectronicServices" : false,
    "ManageInventoryMethodId" : 1,
    "UseMultipleWarehouses" : false,
    "WarehouseId" : null,
    "StockQuantity" : 100000,
    "DisplayStockAvailability" : false,
    "DisplayStockQuantity" : false,
    "MinStockQuantity" : 1000,
    "LowStock" : false,
    "LowStockActivityId" : 2,
    "NotifyAdminForQuantityBelow" : 100000,
    "BackorderModeId" : 0,
    "AllowBackInStockSubscriptions" : false,
    "OrderMinimumQuantity" : 1,
    "OrderMaximumQuantity" : 1,
    "AllowedQuantities" : "1",
    "AllowAddingOnlyExistingAttributeCombinations" : false,
    "NotReturnable" : true,
    "DisableBuyButton" : false,
    "DisableWishlistButton" : false,
    "AvailableForPreOrder" : true,
    "PreOrderAvailabilityStartDateTimeUtc" : ISODate("2018-02-19T19:00:00.000Z"),
    "CallForPrice" : false,
y    "Price" : 1000.0,
    "OldPrice" : 0.0,
    "ProductCost" : 0.0,
    "CustomerEntersPrice" : false,
    "MinimumCustomerEnteredPrice" : 0.0,
    "MaximumCustomerEnteredPrice" : 1000.0,
    "BasepriceEnabled" : false,
    "BasepriceAmount" : 0.0,
    "BasepriceUnitId" : "5a90114d77eb6c43c8f01dca",
    "BasepriceBaseAmount" : 0.0,
    "BasepriceBaseUnitId" : "5a90114d77eb6c43c8f01dca",
    "UnitId" : null,
    "MarkAsNew" : true,
    "MarkAsNewStartDateTimeUtc" : ISODate("2018-02-25T18:00:00.000Z"),
    "MarkAsNewEndDateTimeUtc" : ISODate("2018-02-27T18:00:00.000Z"),
    "HasTierPrices" : false,
    "Weight" : 0.0,
    "Length" : 0.0,
    "Width" : 0.0,
    "Height" : 0.0,
    "AvailableStartDateTimeUtc" : ISODate("2018-02-25T18:00:00.000Z"),
    "AvailableEndDateTimeUtc" : ISODate("2019-02-25T18:00:00.000Z"),
    "DisplayOrder" : 0,
    "DisplayOrderCategory" : 0,
    "DisplayOrderManufacturer" : 0,
    "Published" : true,
    "CreatedOnUtc" : ISODate("2018-02-26T12:27:33.943Z"),
    "UpdatedOnUtc" : ISODate("2018-02-26T12:49:40.794Z"),
    "Sold" : 0,
    "Viewed" : NumberLong(3),
    "OnSale" : 0,
    "Locales" : [ 
        {
            "_id" : "5a940264b0189b0f384a5c7c",
            "LanguageId" : "5a90114d77eb6c43c8f01dd6",
            "LocaleKey" : "SeName",
            "LocaleValue" : "virus-protection"
        }, 
        {
            "_id" : "5a940264b0189b0f384a5c7f",
            "LanguageId" : "5a93f739b0189b0f384a304d",
            "LocaleKey" : "SeName",
            "LocaleValue" : "virus-protection"
        }
    ],
    "ProductCategories" : [],
    "ProductManufacturers" : [],
    "ProductPictures" : [],
    "ProductSpecificationAttributes" : [ 
        {
            "_id" : "5a940c06b0189b0f384a8ce8",
            "AttributeTypeId" : 10,
            "SpecificationAttributeId" : "5a93fffdb0189b0f384a5a7d",
            "SpecificationAttributeOptionId" : null,
            "CustomValue" : "40 kb",
            "AllowFiltering" : false,
            "ShowOnProductPage" : true,
            "DisplayOrder" : 0
        }
    ],
    "ProductTags" : [],
    "ProductAttributeMappings" : [],
    "ProductAttributeCombinations" : [],
    "TierPrices" : [],
    "AppliedDiscounts" : [],
    "ProductWarehouseInventory" : [],
    "CrossSellProduct" : [],
    "RelatedProducts" : []
}

*/
