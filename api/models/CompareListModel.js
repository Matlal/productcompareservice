'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var	ProductModel = require('./ProductModel');
 //var ProductModel = mongoose.model('Product');
  
var CompareListSchema = new Schema({
	  //could be cookie id or somthing else instead
		userId : {type: String,
				unique: true,
				/* validate: {
					validator: function(v, cb) {
						CompareLists.find({userId: v}, function(err,docs){
							cb(docs.length == 0);
						});
					},
					message: 'CompareList already exists for this user id!'
				}, */
				required: "Define user"},
		CreatedAt : {type: Date, default: Date.now},
		UpdatedAt : {type: Date, default: Date.now},
		productIds :  [{type: String/* ,
						validate: {
							validator: function(u,p, cb) {
								CompareLists.find({userId: u}, function(err,docs){
									if(docs.length != 0) {cb(false, "User does not exist");}else{
									docs.productIds.find({userId: p})
									}
								});
							},
							message: 'User does not exist or the product is not unique!'
						} */
					}]
}, { collection: 'CompareLists' });


CompareListSchema.methods.findProductByproductId = 
function( _productId){
	 this.productIds.findOne({productId: _productId}, 
	function(err, _theProduct){
		if(err) return false;	// product does not exist for this list
		return _theProduct;
	}); 
	
};


module.exports = mongoose.model('CompareList', CompareListSchema);



