'use strict';
var mongoose = require('mongoose'),
	ProductModel = mongoose.model('Product'),
	CompareListCtrl = require('./CompareListController');/* ,
  ctrlHelper = require("../helpers/controllerHelper") */;

//GET all
exports.list_all_products = function(req, res) {
  ProductModel.find({}, function(err, product) {
    if (err) {res.status(500).send(err); //res.json(500, { message: err });
    }else{res.json(product);}
  });
};

exports.create_a_product = function(req, res) {
	//TODO validate incoming product: UpdatedAt is a (ISO)Date ?

	//console.log("id:"+req.body.productId + "\n date:"+req.body.UpdatedAt);
	if(!req.body.UpdatedAt||!req.body.productId){ res.status(404)
		.send("invalid post");}
	else{ProductModel.findOne({"productId": req.body.productId},
		function(err, product){
		if(err) {res.status(400).send("not found");}else{
			var date=new Date(req.body.UpdatedAt);
			if(product){
			var dateExist = product.UpdatedAt.toISOString();
			var dateIn = date.toISOString();
				if(product.UpdatedAt.toISOString()!=date.toISOString()){
					product.UpdatedAt = date;
					product.save(function(err, product) {
						if (err){ res.status(400).send(err);}else{
						res.json(product);}
					});}else{res.json(product);}
			}if(!product){
				var new_product = new ProductModel({productId:req.body.productId,
				  UpdatedAt:date});
				new_product.save(function(err, product) {
				if (err){ res.status(400).send(err);}else{
				res.json(product);}});
			}
		//else{console.log("hello2"+product); res.json(product);}
		}
		});
	}
};

var findProductID = function(_productId){
	ProductModel.find({"productId": _productId}, 
	function(err, product){
	if(err||!product) return false;
	return product._id;
	});
};

//GET one
exports.read_a_product = function(req, res) {
	
	ProductModel.find({"productId": req.params.productId},
	function(err, product){
		if(err||!product) {console.log("hello"+product); res.status(400)
			.send("not found");}
		else{console.log("hello2"+product); res.json(product);}
	});
};

//PUT
exports.update_a_product = function(req, res) {
	if(!req.params.productId||!req.body.UpdatedAt) {res.status(404)
		.send("Bad Request");}
	else{
		productExist(req.params.productId, function(err, exist){
			if(err||!exist) res.status(404).send(err||"product do not exist");
			/* console.log("Compare list exist = "+exist ); */
			if(exist){
				console.log(req.body);
				ProductModel.findOneAndUpdate({"productId": 
				req.params.productId}, req.body, {new: true},
				function(err, product) {
					if (err||!product){console.log(product);res.status(400)
						.send(err||"could not found Product");}else{	
					res.json(product);}
				});
			}
		});		
	}
};
//DELETE
exports.delete_a_product = function(req, res) {
  ProductModel.remove({ productId: req.params.productId },
  function(err, product) {
    if (err){ res.status(404).send(err);}
	else{
		if(!CompareListCtrl.deleteProductFromAllLists(req.params.productId)){ 
		res.status(500).send("could not delete products from copmparelists");}
		else{
		res.json({ message: 'Product successfully deleted' });}
	}
  });
};

var productExist= function (_productId, cb){
	ProductModel.find({productId: _productId}, function(err,docs){
		cb(err, docs.length != 0);
	});
};