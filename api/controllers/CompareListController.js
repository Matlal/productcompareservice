'use strict';
var mongoose = require('mongoose');
var CompareListModel = mongoose.model('CompareList'),
  ProductModel = mongoose.model('Product'),
  ProductCtrl = require('./ProductController');
  
/*TODO watch for monoliths flag that disables comparing
On every post for compare list:
 - check values (id, updatedOn[string/dateTime]) 
	if not sync for specified object
		=> send back error message ( "not sync")
		=> await update on product (PUT)
if product does not exist in Microservice
	=>send back error message ( "not found")
		=> await creation for product (POST) 
TODO fix error codes
*/

exports.getAllCompareLists = function(req, res) {
  CompareListModel.find({}, function(err, CompareLists) {
    if (err) res.send(err); //res.json(500, { message: err });
    console.log("loading all compare lists");
	res.json(CompareLists);
  });
};

exports.getCompareList = function(req, res) {
	CompareListModel.findOne({ userId: req.params.userId },
	function(err, compareList) {
    if (err||!compareList){ res.status(404).send(err||"no compare list with "+
	"the user id #"+ req.params.userId + " was found!");} //not found
	else{
	var msg = compareList? "loading compare list #"+compareList._id+
	" with userid #"+compareList.userId : "no compare list with the user id #"+
	req.params.userId + " was found!";
	console.log(msg);
    res.json(compareList);}
  });
};

exports.createCompareList= function(req, res) {
	var _userId=req.body.userId;
	compareListExist(_userId, function(err, exist){
	if(err||exist)res.status(404).send(err||"comparelist do exist already");
	/* console.log("Compare list exist = "+exist ); */
	if(!exist) {
		var userCompareList = createCompareListHelper(_userId);
		res.json(userCompareList);
		}
	});
};

exports.deleteCompareList = function(req, res) {
 CompareListModel.findOneAndRemove({ userId: req.params.userId},
 function(err, compareList) {
  if (err||!compareList) {res.status(404).send(err||"no compare list with "+
	" the user id #"+ req.params.userId + " was found for deletion!"); } else{
  res.json({ message: 'CompareList successfully deleted!' });}
  }); 
};

exports.addProductInCompareList = function(req, res) {
	
	if(!req.body.UpdatedAt||!req.params.userId){ 
		res.status(404).send("invalid post");}
	else{
		ProductModel.findOne({productId:req.params.productId},
		function(err, prod) {
			if (err){res.status(500).send(err);}else{
			if(!prod){var new_product = new ProductModel({productId:req.params.productId, UpdatedAt:req.body.UpdatedAt});
				new_product.save(function(err, product) {
				if (err){ res.status(400).send(err);}
				});
				prod=new_product;
			}
			var date= new Date(req.body.UpdatedAt).toISOString();
			var _date = prod.UpdatedAt.toISOString();
			/* console.log(date +" == "+_date +" = "+(date==_date)); */
			if(date!=_date){ res.status(404).send("product not in sync");}
			else{
				compareListExist(req.params.userId, function(err, exist){
					if(err) {res.status(404).send(err);}
					if(!exist) {
					var userList=createCompareListHelper(req.params.userId);
					console.log("created: "+userList); 
					}
	productInCompareListExist(req.params.userId, req.params.productId,
	function(err, exist){
		/* console.log(exist); */
	if(err||exist){res.status(404).send(err||"product does already exist.");}
	else{
		CompareListModel.findOneAndUpdate({"userId": req.params.userId},
		{$push: {"productIds": req.params.productId}},{new: true},
		function(err, compareList) {
		if(err){res.status(500).json(err);}
		else{
			res.status(200).json(compareList);
			}
		});
	}
	});
				});
			}
			}
			
		});
	}
};
/* exports.updateProductInCompareList = function(req, res) {
  CompareListModel.findOneAndUpdate({_id: req.params.productId},
  req.body, {new: true}, function(err, product) {
    if (err) {res.send(err);} else{
    res.json(product);}
  });  
}; */

exports.deleteProductFromCompareList = function(req, res) {
	var userCompareList=getUserCompareList(req.params.userId);
	userCompareList.exec(function(err, compareList) {
		if (err||!compareList){console.log(compareList+err);
			res.status(404).send(err||"CompareList does not exist");}	//CompareList does not exist
		else{ 
	CompareListModel.findOneAndUpdate({"userId": req.params.userId},{$pull:
	{"productIds": req.params.productId }}, {new: true}, 
	function(err, compareList) {
			if(err){res.status(500).json(err);}
			else{
			res.status(200).json(compareList);
			}
	});
		}
	});
};

exports.deleteProductFromAllLists = function (_productId){
	var _prodId = _productId;
	var conditions = { }, update = {$pull: {"productIds": _prodId }}
	, options = { multi: true };

	CompareListModel.update(conditions, update, options, function(err){
		if(err) return false;
	});
	return true;
	/* var queryAllUsers = () => {
		//Where User is you mongoose user model
		CompareListModel.find({} , (err, lists) => {
			if(err){res.status(404).send(err);}
			else{
				if(!lists){
					lists.map(list => {
						list.productIds. 
					});
				}
			}
		});
	}; */
};
//helper Methods
var getUserCompareList = function(_userId){
	return CompareListModel.findOne({userId:_userId});
};
var createCompareListHelper = function (_userId){
	var userCompareList= new CompareListModel({userId:_userId });
	userCompareList.save(function(err, comparelist) {
		if (err) return {error: "Something when wrong in saving the new "+
		"Compare List, please check that the incoming values have the correct"+
		"format and try again. "+err};
	});
	return userCompareList;
}; 
//validation methods
var compareListExist= function (_userId, cb){
	CompareListModel.find({userId: _userId}, function(err,docs){
		cb(err, docs.length != 0);
	});
};
var productInCompareListExist= function (_userId,_prodId, cb){
	CompareListModel.find({userId: _userId, productIds:_prodId}, 
	function(err,docs){
		cb(err, docs.length != 0);
	});
};

/* exports.add_to_compare_list = function(req,res){
	var id=req.params.productId
	if(!productExist(id)){res.status(404);}
	else{
		var updatedOn = req.body.updatedDate
		if(!isProductInSync(updatedOn)){res.json(500, { message: err });}
		else{
			// add productid to compare list for specified user
			}
	}	
	//var customer_id = req.body.sessionCookie
	//Todo: read customer identifier
}; */