'use strict';
module.exports = function(app) {


// CompareListCtrl Routes
	
var CompareListCtrl = require('../controllers/CompareListController');

  /*Options:
  -GET comparelist
  -POST one product to compare list
  -UPDATE one product in compare list
  -DELET one product in compare list
	*/
  
  // productCtrl Routes
  app.route('/product-compares')
    .get(CompareListCtrl.getAllCompareLists)
    .post(CompareListCtrl.createCompareList);
	
  app.route('/product-compares/:userId')
    .get(CompareListCtrl.getCompareList)
    .delete(CompareListCtrl.deleteCompareList);
	
  app.route('/product-compares/:userId/:productId')
	.post(CompareListCtrl.addProductInCompareList)
	/* .put(CompareListCtrl.updateProductInCompareList) */
	.delete(CompareListCtrl.deleteProductFromCompareList);
	
};