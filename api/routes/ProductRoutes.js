'use strict';
module.exports = function(app) {
  var productCtrl = require('../controllers/ProductController');

  // productCtrl Routes
  app.route('/products')
    .get(productCtrl.list_all_products)
    .post(productCtrl.create_a_product);
	

  app.route('/products/:productId')
    .get(productCtrl.read_a_product) 
    .put(productCtrl.update_a_product)
    .delete(productCtrl.delete_a_product);
	
};