/*jslint devel: true, node: true*/
var environment=process.env.NAME||"Development";
//if(environment !== 'Production') {
	require('dotenv').config();
//}
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var	dbConfig = require('./config/database-config.js');
var ProductModel = require('./api/models/ProductModel');
var	CompareListModel = require('./api/models/CompareListModel');
var app = express();

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url);
//var db=mongoose.createConnection(); 
//for multiple connections (diffrent permissions)

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse requests of content-type - application/json
//app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.json());

var ProductRoutes = require('./api/routes/ProductRoutes'); 
var CompareListRoutes = require('./api/routes/CompareListRoutes'); 
ProductRoutes(app); 
CompareListRoutes(app);

/* var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongoose connection error:'));
db.once('open', function() {
	console.log("mongoose connected!");
}); */

//Testing
/*var kittySchema = mongoose.Schema({name: String});
var Kitten = mongoose.model('Kitten', kittySchema);
app.get('/', function(req, res){
	console.log("trying to find kittens");
	Kitten.find(function (err, docs) {
		if (err){ 
			console.error(err);
			res.status(500).send('Something broke!')
		}
		console.log(docs);
		res.json(docs);
	});
});*/

var port =process.env.PORT || 3000;
app.listen(port, function(){
	console.log('Example app listening on port '+ port);
});

