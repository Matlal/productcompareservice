# create a file named Dockerfile

FROM mhart/alpine-node:latest
#FROM heroku/heroku:16

#Taking advantage of image layers caching

ADD package.json /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/

WORKDIR /opt/app
ADD . /opt/app

EXPOSE 3000

CMD ["npm", "start"]