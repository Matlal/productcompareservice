var environment=process.env.NAME;
var connectionString;

console.log('Starting environment: ' + environment);

switch(environment) {
	case "Production": // External Database
        connectionString=process.env.MONGOLABURI;
        break;
    case "Test": // Internal/local Database 
	connectionString=process.env.MONGODOCKERURI; 
        break;
    default: //Development local
		connectionString= process.env.MONGODEVURI;
}
//console.log('string: ' + connectionString);
module.exports = 
{
	url: connectionString
}


