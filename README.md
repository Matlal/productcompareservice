# ProductCompareService
A microservice for comparing products. It procides a HTTP RESTful API that the GrandNode monolith app can use when a user is comparing products.

# Purpose
This is the implementation of the thesis to compare monolithic programming to microservice programming in it's Scalability. The Monolithic part is not included in this repository because of it's simplicity. 
If your are interested to read the complete thesis, please see the link below but noitce that it's mainly written in Swedish.  

[The Scalability of Microservice Architecture](http://example.com/ "Title")

#Main Branches
*master
*dev